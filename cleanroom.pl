#!/usr/bin/env perl

use strict;
use warnings;

use File::Basename;
use File::Path;
use File::Path qw/make_path/;
use File::Spec;
use File::Spec::Functions qw( abs2rel );

require File::Temp;
use File::Temp ();

use Cwd 'realpath';

use Getopt::Long;

sub is_folder_empty {
    my $dirname = shift;
    opendir(my $dh, $dirname) or die "Not a directory";
    return scalar(grep { $_ ne "." && $_ ne ".." } readdir($dh)) == 0;
}

our $opt_help = 0;
our $opt_debug = 0;
our $opt_stateDir = "";
our $opt_sysconfDir = "";

sub usage() {
    print "cleanroom.pl is used to create a new OS image.\n\n";
    print "Options:\n";
    print "    [--help]                 print these usage instructions.\n";
    print "    [--debug]                print debug information.\n";
    print "    [--statedir=DIR]         set base state directory.\n";
    print "    [--sysconfDir=DIR]       set base system configuration directory.\n";

    exit 0;
}

my $this = $0;
my $cleanroomDir=realpath(dirname($this));
my $sysconfDir=$cleanroomDir;
my $commandsDir="$cleanroomDir/commands";

### option parsing:
GetOptions("help" => \$opt_help,
           "debug" => \$opt_debug,
           "statedir=s" => \$opt_stateDir,
           "sysconfDir=s" => \$opt_sysconfDir) or die "Failed to parse command line.";

usage if $opt_help;

$sysconfDir = $opt_sysconfDir if $opt_sysconfDir;

my $dir = shift;
die "No input directory name to pre-process given." unless $dir;
die "\"$dir\" must be a relative path." if $dir =~ /^\//;
$dir="$sysconfDir/$dir";
die "input directory \"$dir\" does not exist." unless -d $dir;

my $containerFile = "container.conf";

my $input = $dir . "/${containerFile}";
die "Input file \"$input\" not found." unless -e $input;

my $outputDir = $dir . "/.script";
make_path $outputDir or die "Failed to create path: $outputDir";
my $output = "$outputDir/container.sh";

my $relDir = abs2rel($dir, $sysconfDir);
die "$dir is no child of $sysconfDir." if $relDir =~ /^..\//;

my $stateDir = "$cleanroomDir/.script/state";
if ($opt_stateDir) {
    $stateDir = "$opt_stateDir/$relDir/state";
} else {
    $stateDir = "$dir/.script/state";
}
my $postFile = "$stateDir/POST";

$ENV{CONTAINER_DIR} = $dir;
$ENV{CONTAINER_FILE} = $input;
$ENV{CONTAINER_OUT} = $output;
$ENV{CONTAINER_OUT_DIR} = $outputDir;

$ENV{STATE_DIR} = $stateDir;
$ENV{SYSCONF_DIR} = $sysconfDir;
$ENV{CLEANROOM_DIR} = $cleanroomDir;

my $configBaseDir = "${sysconfDir}/cleanroom/config";

my $tmpDirFh = File::Temp::newdir();
my $tmpDir =  $tmpDirFh->dirname;

$ENV{SCRATCH_DIR} = $tmpDir;

if ($opt_debug) {
    print "Sysconf  : $sysconfDir...\n";
    print "CleanRoom: $cleanroomDir...\n";
    print "Dir      : $dir...\n";
    print "Name     : $relDir...\n";
    print "Input    : $input...\n";
    print "Output   : $output (in $outputDir)\n";
    print "State    : $stateDir...\n";
    print "tmp      : $tmpDir...\n";
}

my $fragCount = 0;

sub writeFragment
{
    my $name = shift;
    my $contents = shift;

    $fragCount += 1;
    open(my $fh, ">", "$name") or die "cannot open $outputDir/$name: $!";
    print $fh $contents;
    close $fh;
}

sub lineify
{
    my $file = shift; # filename
    my $input = shift; # a array referrence to all lines of input.

    my $inputLine = 0;
    my $startLine = 0;

    my $buffer = "";

    my @lines;

    my $inFrag = 0;
    my $fragContents = "";
    my $fragName = "";
    my $fragIndent = "";

    foreach my $line (@$input) {
        $inputLine++;
        chomp $line;

        print "<<< $inputLine: $line...$fragName...\n" if $opt_debug;

        if ($inFrag) {
            # Get fragment indentation:
            if (!$fragIndent) {
                $line =~ /^(\s+)/;
                $fragIndent = $1;
                die "Invalid fragmentation in line $inputLine" unless $fragIndent;
            }

            # Process fragment line:

            if ($line =~ /^$fragIndent/) {
                my $c = $line;
                $c =~ s/^$fragIndent//;
                print "    FRAG>>> $c...\n" if $opt_debug;
                $fragContents .= "$c\n";
                next;
            } elsif ($line =~ /^\s*$/) { # also add empty lines!
                print "    FRAG>>> <EMPTY>\n" if $opt_debug;
                $fragContents .= "\n";
                next;
            } else {
                print "    WRITE FRAG>>>$fragContents<<<\n" if $opt_debug;
                writeFragment($fragName, $fragContents);
                $fragContents = "";
                $fragIndent = "";
            }
        }

        print "    line...\n" if $opt_debug;

        $inFrag = 0;

        $line =~ s/^\s*#.*$//;
        $line =~ s/^\s*//;
        $line =~ s/\s*$//;
        next if $line =~ /^$/;

        $fragName = "$outputDir/quote$fragCount.frag";

        # Quote to file:
        if ($line =~ /<<<\s*(.*)\s*>>>$/) {
            writeFragment($fragName, $1);
            $line =~ s/<<<.*/\"$fragName\"/;
        } elsif ($line =~ /<<<$/) {
            $line =~ s/<<<$/\"$fragName\"/;
            $fragContents = "";
            $inFrag = 1;
        }
        # print "Lineify >>> $file:$inputLine: $line...\n" if $opt_debug;
        push @lines, "#$file:$inputLine:$line";
    }

    writeFragment($fragName, $fragContents) if $fragContents;

    return \@lines;
}

my $skipNextRawOutput = 0;

sub parse
{
    my $input = shift; # one line of input in the form "#file:line:text"
    my $pretext = shift;

    die "Unexpected input $input." unless $input =~ /^#([^:]*):(\d+):(.*)$/;
    my $fileName = $1;
    my $lineNo = $2;
    my $text = $3;

    $text =~ s/^\s*//; # Cut leading WS

    my $command = $text;
    $command =~ s/\s.*$//;
    die "What is this text: $text?" unless $command;
    my $args = substr $text, length($command);
    $args = substr $args, 1 if $args;

    $command = uc $command;

    print "L $fileName,$lineNo: $command [ $args ]\n" if $opt_debug;

    my %result;
    $pretext = "" unless $pretext;

    my $elided = "$command $args";
    $elided = substr($elided, 0, 20) . "..." if (length($elided) > 20);

    if ($skipNextRawOutput) { $result{OUTPUT} = ""; }
    else { $result{OUTPUT} = "$pretext# $fileName:$lineNo> $elided\n"; }
    $result{POST} = "";
    $result{PARSE} = "";
    $result{PARSEFILE} = "";

    if ($command eq "POST") {
        die "Error: POST needs an argument to execute.\n" unless $args;
        $result{POST} = $args;
        return \%result;
    }
    if ($command eq "RAW") {
        die "Error: RAW needs an argument to execute.\n" unless $args;
        $skipNextRawOutput = ($args =~ /\\$/);
        $result{OUTPUT} .= $args;
        return \%result;
    }
    my $toRun = "$sysconfDir/cleanroom/commands/$command";
    $toRun = "$commandsDir/$command" unless -f "$toRun";
    if (-f "$toRun") {
        print "Running $toRun $args...\n" if $opt_debug;
        $ENV{SCRIPT} = $toRun;
        $ENV{CONFIG_DIR} = "$configBaseDir/$command";
	$ENV{HELPER_DIR} = "$commandsDir/helper/$command";
        $ENV{STATE_DIR} = "$stateDir";
        $ENV{STAMP} = "$stateDir/CMD-$command.stamp";

        my $output = `$toRun $args`;
        if ($? == 0) {
            $result{PARSE} = $output;
            $result{PARSEFILE} = "$command" if $output;
        } else { die "$fileName:$lineNo: Running $toRun failed:\n$output\n\n"; }
        return \%result;
    } else {
        die "$fileName:$lineNo: Unknown command: \"$command\" with $args\n";
    }
    return \%result;
}

sub parseLines
{
    my $file = shift;
    my $input = shift; # a array referrence to all lines of input.
    my $pretext = shift;

    my @lines = @{lineify($file, $input)};

    my @cmds;
    foreach my $line (@lines) {
        my $cmd = parse($line, $pretext);
        if ($cmd->{PARSE}) {
            die "Asked to parse something, but no file given." unless $cmd->{PARSEFILE};
            my @sublines = split(/\n/, $cmd->{PARSE});
            push @cmds, @{parseLines($cmd->{PARSEFILE}, \@sublines, $cmd->{OUTPUT})};

            $cmd->{PARSE} = "";
            $cmd->{PARSEFILE} = "";
            # fallthrough: Just in case the command has other parts...
        } else {
            die "PARSEFILE found where no PARSE was given." if $cmd->{PARSEFILE};
            push @cmds, $cmd;
        }
    }

    return \@cmds;
}

print "\n\n\n";
print "########################################################################\n";
print "########################################################################\n";
print "### Preprocessing $input\n";
print "### => $output\n";
print "### in $relDir\n";
print "########################################################################\n";
print "########################################################################\n\n";

die "$output was already created." if -e "$output";

### Read input:

open(IN, "<$input") or die "Failed to open $input for reading: $!.\n";

print "Reading $input...\n";
my @lines = <IN>;
my @cmds = @{parseLines($containerFile, \@lines)};

print "Closing $input...\n";
close IN;

open(OUT, ">$output") or die "Failed to open $output for writing: $!.\n";

print "Writing $output...\n";

print OUT "#!/usr/bin/bash\n";
print OUT "set -e\n";
print OUT "set -x\n" if $opt_debug;

print OUT "# Setting up environment for all scripts:\n";
print OUT "export CONTAINER_DIR=\"$dir\"\n";
print OUT "export CONTAINER_FILE=\"$input\"\n";
print OUT "export CONTAINER_OUT=\"$output\"\n";
print OUT "export STATE_DIR=\"$stateDir\"\n";
print OUT "export SYSCONF_DIR=\"$sysconfDir\"\n";
print OUT "export CLEANROOM_DIR=\"$cleanroomDir\"\n";
print OUT "export ENVSUBST_VARS='\${CONTAINER_DIR} \${CONTAINER_FILE} \${CONTAINER_OUT} \${STATE_DIR} \${SYSCONF_DIR} \${CLEANROOM_DIR}'\n";

my @postLines;
foreach my $cmd (@cmds) {
    print OUT $cmd->{OUTPUT} . "\n" if $cmd->{OUTPUT};

    push @postLines, $cmd->{POST} if $cmd->{POST};

    die "PARSE statement found where none should exist anymore." if $cmd->{PARSE};
    die "PARSEFILE statement found where none should exist anymore." if $cmd->{PARSEFILE};
}

print "Checking for POST file...\n";
my @allPostLines;
if (-e $postFile) {
    print "Reading post commands:\n";
    open(POST_IN, "<$postFile") or die "Failed to open $postFile for reading: $!.\n";
    @allPostLines = <POST_IN>;
    close POST_IN;
}

unshift @allPostLines, @postLines;

if (@allPostLines) {
    print "Writing post commands:\n";
    mkdir $stateDir;
    open(POST_OUT, ">$postFile") or die "Failed to open $postFile for writing: $!.\n";
    foreach my $l (@allPostLines) { print POST_OUT "$l\n"; }
    close POST_OUT;

    my @postCmds = @{parseLines("POST", \@allPostLines)};
    foreach my $cmd (@postCmds) {
        print OUT $cmd->{OUTPUT} . "\n" if $cmd->{OUTPUT};

        die "POST statement found where none should exist anymore." if $cmd->{POST};
        die "PARSE statement found where none should exist anymore." if $cmd->{PARSE};
        die "PARSEFILE statement found where none should exist anymore." if $cmd->{PARSEFILE};
    }
}

my $testDir = "$cleanroomDir/tests";
my $test = "";
$test .= `cat "$testDir/"*` if (-d $testDir and !is_folder_empty($testDir));
$testDir = "$sysconfDir/cleanroom/tests";
$test .= `cat "$testDir/"*` if (-d $testDir and !is_folder_empty($testDir));
if ($test) {
    $test = "# TEST SECTION\n$test";
} else {
    $test .= "########## NO TESTS ##########\n";
    print "\n\n\n!!!!!!! NO TESTS !!!!!!!\n\n\n";
}


print "Writing test commands:\n";
print OUT $test;
print OUT "\n# END OF TEST SECTION\n";

print "Closing $output...\n";
close OUT;

chmod 0755, $output or die "Failed to mark $output as executable.";

exit 0;
