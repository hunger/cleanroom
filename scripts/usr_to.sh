#!/usr/bin/bash

cd /mnt/btrfs/playground || exit 42

set -e

IMAGE_NAME="$1"

test -n "${IMAGE_NAME}" || exit 1
HOST=$(echo "${IMAGE_NAME}" | sed -e "s/^system-//")
test -n "${HOST}" || exit 1
ping -c1 -n "${HOST}" > /dev/null 2>&1 || exit 2

echo "Processing ${IMAGE_NAME}..."
IMAGE_ROOT=$(readlink $IMAGE_NAME:latest)
test -d "$IMAGE_ROOT" || exit 2

TS=$(echo "${IMAGE_ROOT}" | sed -e "s/^system-${HOST}-//")

USR_NAME="usr:org.archlinux.hunger:x86_64:$TS"
DEST_DIR="/mnt/btrfs"
INCOMING_DIR="${DEST_DIR}/.incoming"

USR="${IMAGE_ROOT}/${USR_NAME}"
DEST="${DEST_DIR}/${USR_NAME}"
INCOMING="${INCOMING_DIR}/${USR_NAME}"

ssh "${HOST}" mkdir -p "${INCOMING_DIR}"

ssh "${HOST}" btrfs subvolume create "${INCOMING}"
ssh "${HOST}" mkdir -p "${INCOMING}/usr"
tar -cpf - -C "${IMAGE_ROOT}/usr" . | ssh "${HOST}" tar -xpf - -C "${INCOMING}/usr"

ssh "${HOST}" btrfs subvolume snapshot -r "${INCOMING}" "${DEST}"
ssh "${HOST}" btrfs subvolume delete "${INCOMING}"

# update boot loader:
ssh "${HOST}" "${DEST}/usr/local/bin/update-boot.pl" --verbose

# update containers:
ssh "${HOST}" test -x "${DEST}/usr/local/bin/update-all-containers.sh" \
    && ssh "${HOST}" "${DEST}/usr/local/bin/update-all-containers.sh"

# Show diffs:
ssh "${HOST}" test -x "${DEST}/usr/local/bin/compare-usr-images.pl" \
    && ssh "${HOST}" "${DEST}/usr/local/bin/compare-usr-images.pl" current "${TS}"
