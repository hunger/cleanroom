#!/usr/bin/bash

cd /mnt/btrfs/playground || exit 42

set -e

HOST="$1"
IMAGE="$2"

test -n "$HOST" || exit 1
ping -c1 -n "$HOST" > /dev/null 2>&1 || exit 2

echo "Processing $IMAGE..."
test -n "$IMAGE" || exit 1
VM=$(readlink $IMAGE:latest)
test -d "$VM" || exit 2

DEST=/var/lib/machines
IN="${DEST}/.incoming"
ARCH="${DEST}/.archive"
FIN="${IN}/${VM}"

BASE=$(echo "$IMAGE" | sed -e "s/^system-//")
FDEST="${DEST}/${BASE}"

ADEST="${ARCH}/${VM}"

if ssh "$HOST" "test -d \"${ADEST}\"" ; then
    echo "Already installed"
    exit 1
fi

ssh "$HOST" mkdir -p "${IN}"
ssh "$HOST" mkdir -p "${ARCH}"

# Do not use btrfs send as /usr is a subvol itself!
ssh "$HOST" btrfs subvol create "${FIN}"

tar -cpf - -C "${VM}" . | ssh "$HOST" tar -xpf - -C "${FIN}"

ssh "$HOST" "test -d \"${FDEST}\" && btrfs subvol delete \"${FDEST}\"" || /usr/bin/true
ssh "$HOST" "btrfs subvol snapshot -r \"${FIN}\" \"${FDEST}\""
ssh "$HOST" "btrfs subvol snapshot -r \"${FIN}\" \"${ADEST}\""
ssh "$HOST" "btrfs subvol delete \"${FIN}\""

