#!/usr/bin/perl

use strict;
use warnings;

use File::Copy qw( copy );
use File::Path qw( make_path );
use File::Find qw( finddepth );
use File::Spec::Functions qw( rel2abs );
use Cwd qw( abs_path );
use Getopt::Long;

my $BTRFS_CONTROL="/mnt/btrfs";
my $BOOT="/boot";

my $opt_help = 0;
my $opt_verbose = 0;
my $opt_keep = 0;
my $opt_force = 0;

my $opt_vendor = 'org.archlinux.hunger';
my $opt_arch = 'x86_64';

my $rootDev = '';
$rootDev = `cat "/usr/lib/modules/boot/root_device"`
    if -e "/usr/lib/modules/boot/root_device";
chomp $rootDev;

sub verbose {
    my $msg = shift;
    print "$msg\n" if $opt_verbose;
}

sub setupBtrfsDir {
    my $baseDir = shift;
    die "not enough parameters." unless $baseDir;

    my @mounts = `mount`;
    ### find the btrfs root:
    unless ($rootDev) {
        foreach my $l (@mounts) {
            next unless $l =~ / on \/ type /;
            $rootDev = $l;
            $rootDev =~ s/ on \/ type .*$//;

            die "Root filesystem is not btrfs!" unless $l =~ / on \/ type btrfs /;
        }
        chomp $rootDev;
    }

    die "Root device not found!" unless $rootDev;
    verbose("Root device is $rootDev.");

    ### Mount btrfs control dir:
    my $controlMounted = 0;
    my $realRootDev = readlink $rootDev;
    my $baseRoot = $rootDev;
    $baseRoot =~ s!/[^/]*$!!;
    $realRootDev = abs_path(rel2abs($realRootDev, $baseRoot));
    print "real rootdev: $realRootDev...\n";
    foreach my $l (@mounts) {
        next unless $l =~ / on $BTRFS_CONTROL type /;
        die "Wrong filesystem mounted on $BTRFS_CONTROL."
	    unless $l =~ /^$realRootDev on / or $l =~ /^$rootDev on /;

        $controlMounted = 1;
        last;
    }

    unless ($controlMounted) {
        unless (-d $BTRFS_CONTROL) {
            make_path($BTRFS_CONTROL) or die "Failed to create $BTRFS_CONTROL.";
        }
        my $output = `mount -t btrfs -o subvolid=0 "$rootDev" "$BTRFS_CONTROL"`;
        die "Failed to mount $rootDev:\n$output" if $?;

        $controlMounted = 1;
    }

    return $baseDir;
}

# handle options:

GetOptions('help!' => \$opt_help,
           'verbose!' => \$opt_verbose,
           'keep!' => \$opt_keep,
           'force!' => \$opt_force,
           'vendor:s' => \$opt_vendor,
           'arch:s' => \$opt_arch,
           'rootdev:s' => \$rootDev);

# validate options:
if ($opt_help) {
    print "$0 [Options] image\n\n";
    print "Options are:\n";
    print "    --help               print this help text\n";
    print "    --verbose            produce more output\n\n";
    print "    --keep               keep temporary files and mounts (default: no)\n";
    print "    --force              do not break (default: no)\n\n";
    print "    --vendor=<string>    vendor id (default: $opt_vendor)\n";
    print "    --arch=<string>      arch (default: $opt_arch)\n\n";
    print "    --rootdev=<string>   root device (default: autodetect)\n\n";
    exit 0;
}

verbose(">>> Root device: $rootDev.\n");

# Sanity check environment:
my $efiLinux = "$BOOT/EFI/Linux";
`mount -t vfat /dev/disk/by-partlabel/EFI\\\\x20System "$BOOT"` unless -d $efiLinux;
die "No EFI/Linux folder found in $BOOT." unless -d $efiLinux;

# Set up directory:
my $btrfsDir = setupBtrfsDir($BTRFS_CONTROL);
die "No btrfs directory." unless $btrfsDir;

# find usr partitions to install:

my @usrs;
my %aliases;

opendir(my $dh, $btrfsDir) || die;
while(readdir $dh) {
    my $dir = "$_";
    my $fullDir = "$btrfsDir/$dir";
    $fullDir = "$fullDir/usr" if -d "$fullDir/usr";
    next unless -d "$fullDir";

    if ($dir =~ /^usr:$opt_vendor:$opt_arch:([a-zA-Z0-9_-]+)$/) {
	if (!-f "$fullDir/lib/modules/boot/linux.efi") {
            verbose(">>> Skipping usr $dir: No linux.efi found in /lib/modules/boot.");
            next;
        }
        verbose(">>> Registering $dir as usr $1.");
        push @usrs, $1;
        next;
    }
}
closedir $dh;

die "No usr snapshots found." unless @usrs;

# Install:

## copy kernels/initrds:
verbose("Copy kernels.");
my %usr;

my %keepers;

my $rootDevId = `blkid "$rootDev"`;
die "Failed to get root UUID." if $?;

$rootDevId =~ s/^.*\sUUID=\"/UUID=/;
$rootDevId =~ s/\s+.*$//;
$rootDevId =~ s/[^UID=a-f0-9-]//g;
verbose("      Root Device UUID: $rootDevId.");

foreach my $u (sort @usrs) {
    my $shortDir = "usr:$opt_vendor:$opt_arch:$u";
    my $usrDir = "$btrfsDir/$shortDir";
    $usrDir = "$usrDir/usr" if -d "$usrDir/usr";
    my $bootDataDir = "$usrDir/lib/modules/boot";

    # EFI boot:
    die "linux.efi not found in $bootDataDir." unless -e "$bootDataDir/linux.efi";

    make_path($efiLinux) or die "Failed to create $efiLinux: $!."
        unless -d $efiLinux;

    verbose(">>> Installing EFI kernel linux-$u.efi");
    copy("$bootDataDir/linux.efi", "$efiLinux/linux-$u.efi")
        or die "Failed to copy $bootDataDir/linux.efi: $!.";

    $keepers{"$efiLinux/linux-$u.efi"} = 1;

    next;
}

### Clean up:
sub is_folder_empty {
    my $dirname = shift;
    return 0 if $dirname eq "/boot/loader/entries";
    opendir(my $dh, $dirname) or die "Not a directory";
    return scalar(grep { $_ ne "." && $_ ne ".." } readdir($dh)) == 0;
}

sub cleanup
{
    my $path = $File::Find::name;
    if (-f and !$keepers{$path}) { 
        verbose("    Deleting file $path.");
        unlink($File::Find::name) unless $opt_keep;
    }
    if (-d and is_folder_empty($path)) {
        verbose("     Deleting directory $path.");
        rmdir($File::Find::name) unless $opt_keep;
    }
}

verbose("Cleaning up...");
my @toClean;

push(@toClean, "${BOOT}/EFI/Linux") if -d "${BOOT}/EFI/Linux";

verbose(">>> Cleaning up " . join(", ", @toClean) . "...\n");
finddepth(\&cleanup, @toClean) if @toClean;
