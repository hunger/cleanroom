#!/usr/bin/perl -w

use strict;
use warnings;

my $old = shift;
my $new = shift;

die "Too many arguments." if @ARGV;
die "Two images needed." unless $old;

sub dirFromId {
    my $id = shift;
    die "No id given." unless $id;

    return $id if -d $id;

    my $loc = "/mnt/btrfs/usr:org.archlinux.hunger:x86_64:$id";
    return $loc if -d $loc;

    if ($id eq "latest") {
        my $loc = `ls -d /mnt/btrfs/usr\:org.archlinux.hunger\:x86_64\:* | sort | tail -n 1`;
        chomp $loc;
        return $loc if $loc;
    }

    if ($id eq "current") {
        $loc = `grep -e "^VERSION_ID=" /etc/os-release`;
        $loc =~ s/^VERSION_ID=\"//;
        $loc =~ s/\"$//;
        chomp $loc;

        die "VERSION_ID in /etc/os-release is invalid." if $loc eq "current";
        return dirFromId($loc) if $loc;
    }

    die "Could not find directory for id $id: $loc does not exist.";
}

sub longestStringLength {
    my $max = 5;
    for (@_) {
        next unless $_;
        if (length > $max) { # no temp variable, length() twice is faster
            $max = length;
        }
    }
    return $max;
}

my $oldDir = dirFromId($old);
my $newDir = dirFromId($new);

$old = $oldDir;
$old =~ s/^.*://;
$new = $newDir;
$new =~ s/^.*://;

print "Comparing $old to $new:\n\n";

if ($old eq $new) {
    print "**** Same version, no difference ****\n\n";
    exit 0
}

my %packages;

$oldDir = "$oldDir/usr" if -d "$oldDir/usr";
$newDir = "$newDir/usr" if -d "$newDir/usr";

# Get package list data from old and new directory:
my $list = `pacman --dbpath "$oldDir/lib/pacman/db" -Q`;
foreach my $l (split "\n", $list) {
    ( my $pkg, my $version ) = split /\s+/, $l;
    $packages{$pkg}{old} = $version;
}

$list = `pacman --dbpath "$newDir/lib/pacman/db" -Q`;
foreach my $l (split "\n", $list) {
    ( my $pkg, my $version ) = split /\s+/, $l;
    $packages{$pkg}{new} = $version;
}

# find new/deleted/modified packages:
my @pkgs;

foreach my $p (sort keys %packages) {
    $packages{$p}{old} = "-" unless defined $packages{$p}{old};
    $packages{$p}{new} = "-" unless defined $packages{$p}{new};
    push @pkgs, $p;
}

my @oldVersions = map { $packages{$_}{old} } (@pkgs);
my @newVersions = map { $packages{$_}{new} } (@pkgs);

my $nameLength = longestStringLength(@pkgs);
my $oldVerLength = longestStringLength(@oldVersions);
my $newVerLength = longestStringLength(@newVersions);

# gather descriptions:
for my $p (@pkgs) {
     next if $packages{$p}{old} eq $packages{$p}{new};

     my $target = $newDir;
     $target = $oldDir unless $packages{$p}{new} ne "-";

     my $desc = `pacman --dbpath "$target/lib/pacman/db" -Qi $p`;
     for my $l (split /\n/, $desc) {
         next unless $l =~ /^Description\s*: /;
         $l =~ s/^Description\s*:\s*//;
         $packages{$p}{desc} = $l;
     }
}

print "New packages:\n";
foreach my $p (@pkgs) {
    next unless $packages{$p}{old} eq "-";
    printf "    %-${nameLength}s: %-${oldVerLength}s -> %-${newVerLength}s (%s)\n", $p, $packages{$p}{old}, $packages{$p}{new}, $packages{$p}{desc};
}

print "\nRemoved packages:\n";
foreach my $p (@pkgs) {
    next unless $packages{$p}{new} eq "-";
    printf "    %-${nameLength}s: %-${oldVerLength}s -> %-${newVerLength}s (%s)\n", $p, $packages{$p}{old}, $packages{$p}{new}, $packages{$p}{desc};
}

print "\nUpdated packages:\n";
foreach my $p (@pkgs) {
    next unless $packages{$p}{old} ne "-" and $packages{$p}{new} ne "-" and $packages{$p}{old} ne $packages{$p}{new};
    printf "    %-${nameLength}s: %-${oldVerLength}s -> %-${newVerLength}s (%s)\n", $p, $packages{$p}{old}, $packages{$p}{new}, $packages{$p}{desc};
}

print "\n";

