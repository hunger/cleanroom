#!/usr/bin/sh

ME=$(basename "$0")
MACHINE=${ME%%-login}

test "${ME}" = "${MACHINE}" && exit 23

ENV="--setenv=DISPLAY=$DISPLAY"

for e in $(grep -v "^#" /etc/environment) ; do
    ENV="${ENV} --setenv=${e}"
done

machinectl ${ENV} shell "${USER}@${MACHINE}" /usr/bin/fish
