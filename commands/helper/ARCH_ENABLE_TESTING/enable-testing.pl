#!/usr/bin/perl -w

use warnings;
use strict;

my $pacconf = shift;
die "No pacman.conf file given to process." unless $pacconf;
die "Too many arguments." if $ARGV[0];

sub enableOrKeep {
   my $isEnabled = shift;
   my $line = shift;

   die "Too many arguments" if shift;
   return $line unless $isEnabled;

   return $line if ($line =~ /^#\s+/);
   $line =~ s/^#//;
   return $line;
}

my %enabledSections;

sub mustEnable {
    my $section = shift;

    return 1 if $section eq "testing" and ($enabledSections{core} or $enabledSections{extra});
    return 0 unless $section =~ /-testing$/;
   
    my $prefix = $section;
    $prefix =~ s/-testing$//;

    return $enabledSections{$prefix};
}

my @lines = split "\n", `cat "$pacconf"`;

foreach my $l (@lines) {
    $enabledSections{$1} = 1 if $l =~ /^\[(.*)\]$/;
}

open(OUT, "> $pacconf") or die "Failed to open $pacconf for writting: $!";

my $forceEnable = 0;
foreach my $l (@lines) {
    $forceEnable = mustEnable($1) if $l =~ /^#\[(.*)\]$/;
    print OUT enableOrKeep($forceEnable, $l) . "\n";
}

print OUT "\n";

close(OUT);
