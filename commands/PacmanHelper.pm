#!/usr/bin/perl

package PacmanHelper;

### Definitions:

use File::Path qw(make_path);

# define confDir based on environment:
my $confDir = $ENV{CONFIG_DIR};
$confDir =~ s!/ARCH_PACMAN!/ARCH_PACSTRAP!;
die "Error: CONFIG_DIR not set." unless $confDir;
die "Error: CONFIG_DIR not valid." unless -d "$confDir";

my $useConf = "$confDir/pacman.conf.use";
my $installConf = "$confDir/pacman.conf.install";

my $dbpath = $ENV{SCRATCH_DIR} . "/pacman";

### Functions:

sub configDirectory {
    return $confDir;
}

sub useConfig {
    return $useConf;
}

sub installConfig {
    return $installConf;
}

sub removePackage {
    my $p = shift;
    return $p =~ /^-/;
}

sub packageName {
    my $p = shift;
    return substr($p, 1) if removePackage($p);
    return $p;
}

sub expandPacmanGroups {
    my $input = shift;
    die ("Not enough parameters!") unless $input;

    unless (-d $dbpath) {
        make_path($dbpath);
        `pacman -Syu --config \"$useConf\" --dbpath \"$dbpath\"`;
    }

    my @inputlist = split /\s+/, $input;

    my %toAddInput;
    my %toRemoveInput;
    foreach my $p (@inputlist) {
	my $pn = packageName($p);
	if (removePackage($p)) { $toRemoveInput{$pn} = 0; }
	else { $toAddInput{$pn} = 0; }
    }
    my $toAddList = join " ", (keys %toAddInput);

    ### Sanity check: No package is to be added and removed at the same time:
    foreach my $p (keys %toAddInput) {
        die "$p is to be installed and not installed at the same time" if $toRemoveInput{$p};
    }

    ### Get data on package groups from pacman
    my %groupData;

    # add group data from pacman
    open(PACMAN, "pacman -Sg --config \"$useConf\" --dbpath \"$dbpath\" $toAddList|");
    while(<PACMAN>) {
        chomp;
        $groupData{$1}{$2} = 1 if ($_ =~ /^([a-zA-Z0-9_-]+) ([a-zA-Z0-9_-]+)$/);
    }
    close(PACMAN);

    # fill in data not returned by pacman
    foreach my $p (keys(%toAddInput))
    { $groupData{$p}{$p} = 1 unless $groupData{$p}; }
    foreach my $p (keys(%toRemoveInput))
    { $groupData{$p}{$p} = 1 unless $groupData{$p}; }

    ### Generate list of all packages (with groups expanded)
    my %result;
    foreach my $p (keys %toAddInput) {
        foreach my $g (keys %{$groupData{packageName($p)}}) {
            $result{$g} = 1;

	    # increment count (to find unused additions later):
	    $toAddInput{$g} += 1;
	    $toAddInput{$p} += 1;
        }
    }

    ### Remove blocked packages:
    foreach my $p (keys %toRemoveInput) {
        if (exists $result{$p}) {
	    delete $result{$p};
	    $toRemoveInput{$p} += 1;
	}
    }

    ### sanity check: Break on unused adds:
    foreach my $p (keys %toAddInput) {
        die "$p was not added." unless ($toAddInput{$p} > 0);
    }
    ### sanity check: Breack on unused removes:
    foreach my $p (keys %toRemove) {
	die "$p was not removed." unless ($toRemoveInput{$p} > 0);
    }

    my @list = sort(keys(%result));
    return join " ", @list;
}

1;
