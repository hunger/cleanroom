#!/usr/bin/env perl

use strict;
use warnings;

die "Error: PKG_NFTABLES does not take parameters." if defined $ARGV[0];

my $base = $ENV{CONTAINER_DIR};
die "Error: CONTAINER_DIR is not set when running PKG_NFTABLES." unless $base;

die "Error: PKG_NFTABLES was already run." if -e "$ENV{STAMP}";
`touch "$ENV{STAMP}"`;

print qq{# Set up nftables:
ARCH_PACMAN <<< nftables >>>

# Configure nftables:
FILE_CREATE "\${ETC}/nftables.conf" <<<
    #!/usr/bin/nft -f

    flush ruleset

    table inet filter {
      set open-tcp-ports {
        type inet_service
      }

      set open-udp-ports {
        type inet_service
      }

      chain input {
        type filter hook input priority 0;

        # bad tcp -> avoid network scanning:
        tcp flags & (fin|syn) == (fin|syn) drop
        tcp flags & (syn|rst) == (syn|rst) drop
        tcp flags & (fin|syn|rst|psh|ack|urg) < (fin) drop # == 0 would be better, not supported yet.
        tcp flags & (fin|syn|rst|psh|ack|urg) == (fin|psh|urg)  drop

        iif lo accept

        # limit icmp
        ip protocol icmp limit rate 10/second accept
        ip protocol icmp drop

        # limit icmp6
        ip6 nexthdr icmpv6 limit rate 10/second accept
        ip6 nexthdr icmpv6 drop

        # Connection tracking:
        ct state invalid drop
        ct state { established, related } accept
        ct state { new } tcp dport \@open-tcp-ports accept
        ct state { new } udp dport \@open-udp-ports accept

        # everything else
        reject with icmp type port-unreachable
        drop
      }

      chain forward {
        type filter hook forward priority 0;
        drop
      }

      chain output {
        type filter hook output priority 0;
        accept
      }
    }
CHMOD 640 /etc/nftables.conf

SYSTEMCTL_ENABLE nftables.service
};
